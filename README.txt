SUMMARY:
===========================
The motive behind the module is to allow user to own the content with admin or
manage rights, If site has created some content and as administrator of site you
are aware about the content is owned by someone else, you want to let someone
own that content or give admin/manage permission. But not sure to whom. At that
time this module can help you.

It will allow you to create / configure a form with fields, for any kind of
entity with any bundle. It will get all the necessary informations that you want
to ask from users and list them for admin approval. If you find the claim is
legit you can approve the claim and assign the new role if you want or the
configured roles will be assigned to user. you can also reject the claim if you
find the details unsatisfied.

REQUIREMENTS
===========================
Entity API
Rules
Views

CONFIGURATIONS:
===========================
- Enable the module.

- Configure the bundle that you want to make available for claim.
    - admin/structure/claims/add

- All enabled bundles will be listed for field configurations.
    - admin/structure/claims

- You can access the claim page for the entity with this url. i.e.
    - claim/add/[entity_type]/[entity_id]


PERMISSIONS:
===========================

Set permission for the user whom you want to give access.

 -- Administer Entity claim module
    Only Administrators should have access to this.

 -- Moderate Entity claim module
    Modify the information filled in claim form.

 -- View any Entity claims
    Allows a user to see claims Regard less of published or not.

A typical permission setup so that a user can take advantage of claim looks like:

Entity Permissions (i.e.):
 -- Node- Basic page : Create  claims
 -- Node- Basic page : View claims
 -- Node- Basic page : Edit own claims
 -- Node- Basic page : Edit any claims

CONTACT
===========================
Current maintainers:

* Mitesh Patel (miteshmap) - https://www.drupal.org/u/miteshmap
