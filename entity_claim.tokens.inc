<?php

/**
 * @file
 * Token callbacks for the Entity claim module.
 */

/**
 * Get all existing claim types and expected data types.
 *
 * @return array
 *  Array of claim types.
 *  Format: entity_type => expected_data_type
 */
function _entity_claim_get_token_types() {
  $types = &drupal_static(__FUNCTION__);

  if (!isset($types)) {
    $types = db_select('entity_claim', 'ec')
      ->fields('ec', array('entity_type'))
      ->distinct()
      ->execute()
      ->fetchCol();

    $types = array_combine(array_values($types), array_values($types));

    //@HACK: For some reasons a token for 'taxonomy_term' is just 'term'.
    if (isset($types['taxonomy_term'])) {
      $types['taxonomy_term'] = 'term';
    }
  }

  return $types;
}

/**
 * Implements hook_token_info().
 */
function entity_claim_token_info() {
  $tokens = array();
  foreach (_entity_claim_get_token_types() as $type => $expected_data_type) {
    $tokens[$type] = array(
      'name' => t('Related %type', array('%type' => $type)),
      'description' => t('Related %type.', array('%type' => $type)),
      'type' => $expected_data_type,
    );
  }

  $tokens['entity_url'] = array(
    'name' => t('Claimed entity URL'),
    'description' => t('URL of the entity claimed by the user.'),
    'type' => 'claim',
  );

  return array(
    'tokens' => array(
      'claim' => $tokens,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function entity_claim_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $output = array();

  if ($type == 'claim' && !empty($data['claim'])) {
    $claim = $data['claim'];

    foreach (_entity_claim_get_token_types() as $type => $expected_data_type) {
      if ($scope_tokens = token_find_with_prefix($tokens, $type)) {
        $entities = entity_load($type, array($claim->entity_id));

        $scope_data = array(
          $expected_data_type => reset($entities),
        );

        foreach (token_generate($expected_data_type, $scope_tokens, $scope_data, $options) as $original => $value) {
          $output[$original] = $value;
        }
      }
    }

    $output['[claim:entity_url]'] = $claim->entity_type . '/' . $claim->entity_id;
  }

  return $output;
}
