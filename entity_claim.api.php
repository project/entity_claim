<?php

/**
 * @file
 * API documentation for entity_claim.
 */

/**
 * Control access to claim an entity.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to claim an entity.
 *
 * @param $entity_type
 *   String entity type.
 * @param $entity_id
 *   Entity ID on which the claim is to be done.
 *
 */
function hook_entity_claim_add_access($entity_type, $entity_id) {

}

/**
 * Allows other modules to alter claim object before it is saved.
 */
function hook_claim_presave($claim) {

}

/**
 * Allows other modules to take some action after a claim is added.
 *
 * @param $claim
 *   Claim object.
 * @param $values
 *   Values array containing the data for the claim.
 */
function hook_claim_insert($claim, $values) {

}

/**
 * Allows other modules to take some action after a claim is updated.
 *
 * @param $claim
 *   Claim object.
 * @param $values
 *   Values array containing the edited fields.
 */
function hook_claim_edit($claim, $values) {

}
