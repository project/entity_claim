<?php

/**
 * @file
 * Metadata controller for Entity Claim.
 */

/**
 * Class EntityClaimMetadataController.
 *
 * Entity Claim utilizes custom property attributes to determine if a property
 * should be available as a filter on the listing page. These attributes are
 *   filter: TRUE to add as a filter.
 *   filter_operator: EFQ supported operators.
 *     Defaults to = or IN depending on value submitted
 *   field_type: textfield, select, etc.
 *   options list: callback that returns the options for this field.
 */
class EntityClaimMetadataController extends EntityDefaultMetadataController {

  /**
   * Overrides entityPropertyInfo().
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['bundle'] = array(
      'label' => t('bundle'),
      'description' => t('The type of the claim.'),
      'type' => 'token',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer entity_claim',
      'options list' => 'entity_claim_bundle_options_list',
      'required' => TRUE,
      'schema field' => 'bundle',
    );

    $properties['created'] = array(
      'label' => t("Created"),
      'description' => t("The date the claim was created."),
      'type' => 'date',
      'schema field' => 'created',
      'setter callback' => 'entity_property_verbatim_set',
    );

    $properties['updated'] = array(
      'label' => t("Updated"),
      'description' => t("The date the claim was updated."),
      'type' => 'date',
      'schema field' => 'updated',
      'setter callback' => 'entity_property_verbatim_set',
    );

    $properties['status'] = array_merge($properties['status'], array(
      'label' => t('Status'),
      'filter' => TRUE,
      'field_type' => 'select',
      'options list' => 'entity_claim_status_options',
      'setter callback' => 'entity_property_verbatim_set',
    ));

    // Entities.
    $properties['claimer'] = array(
      'label' => t("Claimer"),
      'type' => 'user',
      'description' => t("The Drupal user associated with the claimer."),
      'getter callback' => 'entity_claim_claimer_property_user_get',
      'setter callback' => 'entity_claim_claimer_property_user_set',
      'schema field' => 'uid',
    );

    $properties['updated_user'] = array(
      'label' => t("Updated User"),
      'type' => 'user',
      'description' => t("The author who updated the claim."),
      'getter callback' => 'entity_claim_updated_by_property_user_get',
      'setter callback' => 'entity_claim_updated_by_property_user_set',
      'schema field' => 'updated_by',
    );

    $properties['entity'] = array(
      'label' => t("Source entity"),
      'type' => 'entity',
      'description' => t("The related entity."),
      'getter callback' => 'entity_claim_entity_property_source_get',
      'setter callback' => 'entity_property_verbatim_set',
    );

    return $info;
  }

}
