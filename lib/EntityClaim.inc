<?php

/**
 * The class used for entity claim.
 */
class EntityClaim extends Entity {

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, $entityType);
    $this->label = $this->defaultLabel();
    $this->setUp();
  }

  /**
   * Change the default URI from default/id to project/id
   */
  protected function defaultUri() {
    return array('path' => 'claim/' . $this->id);
  }

  public function defaultLabel() {

    if (isset($this->entity_type) && isset($this->entity_id) && isset($this->uid)) {
      $claimed_entities = entity_load($this->entity_type, array($this->entity_id));
      $claimed_entity = $claimed_entities[$this->entity_id];

      $claimed_entity_wrapper = entity_metadata_wrapper($this->entity_type, $claimed_entity);

      $account = user_load($this->uid);

      return t('!name claimed by !user', array(
        '!name' => $claimed_entity_wrapper->label(),
        '!user' => $account->name,
      ));
    }
    else {
      return t('New claim');
    }
  }

  /**
   * Set up the object instance on construction or unserializiation.
   */
  protected function setUp() {
    parent::setUp();
    if (isset($this->entity_type) && isset($this->entity_id)) {
      $sub_entity = entity_load_single($this->entity_type, $this->entity_id);
      $claimed_entity_wrapper = entity_metadata_wrapper($this->entity_type, $sub_entity);
      $this->bundle = _entity_claim_prepare_bundle_name($this->entity_type, $claimed_entity_wrapper->getBundle());
      $this->label = $this->label();
      $this->entity_label = $claimed_entity_wrapper->label();
    }
  }

  public function publish() {
    entity_revision_set_default('claim', $this);
    $this->status = 1;
    $this->save();
  }

  public function unpublish() {
    $this->status = 0;
    $this->save();
  }
}
