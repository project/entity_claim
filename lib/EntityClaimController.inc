<?php
/**
 * @file
 */

/**
 * Entity controller class.
 */
class EntityClaimController extends EntityAPIController {
  public $entity;

  /**
   * If user is no administer or moderator and the claim needs to be moderated.
   *
   * @param object $entity
   *
   * @return bool
   */
  private function _needs_approval($entity) {
    return !user_access('administer entity_claim') && !user_access('moderate entity_claim')
           && !user_access("edit any {$entity->bundle} claims");
  }

  /**
   * Set the claim entity to previous revision.
   *
   * @param object $entity
   *
   * @return bool
   */
  private function setPreviousRevision($entity) {
    $vids = db_select('entity_claim_revision', 'r')
      ->fields('r', array('vid'))
      ->condition('r.id', $entity->id)
      ->condition('r.vid', $entity->vid, '<>')
      ->execute()->fetchCol();

    if (!empty($vids)) {
      $entity_revision = entity_revision_load('claim', max($vids));
      entity_revision_set_default('claim', $entity_revision);
      $entity_revision->save();

      return TRUE;
    }

    return FALSE;
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;

    $entity = (object) $entity;
    $entity->is_new = !empty($entity->is_new) || empty($entity->{$this->idKey});

    $entity->timestamp = REQUEST_TIME;
    // Set the timestamp fields.
    if (empty($entity->created)) {
      $entity->created = REQUEST_TIME;
    }

    // Set the timestamp fields.
    if (empty($entity->updated)) {
      $entity->updated = REQUEST_TIME;
    }

    if ($user->uid != $entity->uid) {
      $entity->updated_by = $user->uid;
    }

    $needs_approval = $this->_needs_approval($entity);

    if ($entity->is_new && $needs_approval) {
      $entity->status = ENTITY_CLAIM_PENDING;
    }
    if (!isset($entity->deleted)) {
      $entity->deleted = 0;
    }


    if ($needs_approval) {
      if (isset($entity->vid)) {
        $entity->old_vid = $entity->vid;
        $entity->is_new_revision = TRUE;
      }
    }

    parent::save($entity);

    // Roll revision back.
    if ($needs_approval && isset($entity->old_vid)) {
      $previous_revisions = $this->load(FALSE, array('vid' => $entity->old_vid));
      $previous_revision = reset($previous_revisions);

      entity_revision_set_default('claim', $previous_revision);

      parent::save($previous_revision);
    }
  }

  public function deleteRevision($revision_id) {
    if ($entity_revision = entity_revision_load($this->entityType, $revision_id)) {
      if (entity_revision_is_default($this->entityType, $entity_revision)) {
        // Try to set previous version or delete.
        $this->setPreviousRevision($entity_revision)
          ? $this->deleteRevision($revision_id)
          : $entity_revision->delete();
      }
      else {
        parent::deleteRevision($revision_id);
      }
    }
  }

  /*public function softDelete($ids, DatabaseTransaction $transaction = NULL) {

    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }
    foreach($entities as $entity) {
      $entity->deleted = TRUE;
      $this->save($entity);
    }
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    if (user_access('administer entity_claim') || user_access('moderate entity_claim')) {
      parent::delete($ids, $transaction);
    }
    else {
      $this->softDelete($ids, $transaction);
    }
  }*/
}
