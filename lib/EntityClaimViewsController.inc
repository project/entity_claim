<?php
/**
 * @file
 * Views controller for Entity Claim.
 */

/**
 * EntityClaim Views Controller class.
 */
class EntityClaimViewsController extends EntityDefaultViewsController {

  /**
   * Add extra fields to views_data().
   */
  public function views_data() {
    return parent::views_data();
  }

}
