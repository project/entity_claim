<?php
/**
 * @file
 * Code for pages callback for entity_claim.
 */

/**
 * Load or create new claim entity.
 *
 * @param $entity_type
 * @param $entity_id
 * @param $op
 *
 * @return object
 *  Claim entity.
 */
function _entity_claim_load_or_create($entity_type, $entity_id, $op = 'edit') {
  $entities = &drupal_static(__FUNCTION__);

  if (empty($entities) || !isset($entities[$entity_type][$entity_id])) {
    global $user;
    if ($op != 'new') {
      $entities_load = entity_load('claim', FALSE, array(
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'uid' => $user->uid,
      ));
      $entities[$entity_type][$entity_id] = reset($entities_load);
    }
    else {
      $entity_wrapper = entity_metadata_wrapper($entity_type, $entity_id);
      $entities[$entity_type][$entity_id] = entity_create('claim', array(
        'uid' => $user->uid,
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'bundle' => _entity_claim_prepare_bundle_name($entity_type, $entity_wrapper->getBundle()),
      ));
    }
  }

  return $entities[$entity_type][$entity_id];
}

/**
 * Menu callback: Create claim form.
 *
 * @param $entity_type
 *  Entity type, i.e. node, comment, user etc..
 * @param $entity_id
 *  Numeric Entitye id.
 *
 * @return mixed
 */
function entity_claim_add_form($form, &$form_state, $entity_type, $entity_id) {
  $claim_entity = _entity_claim_load_or_create($entity_type, $entity_id, 'new');
  return entity_claim_form($form, $form_state, $claim_entity);
}

/**
 * Generates the claim editing form.
 */
function entity_claim_form($form, &$form_state, $entity = NULL, $op = 'new', $entity_type = NULL) {
  field_attach_form('claim', $entity, $form, $form_state);

  if (isset($entity->id)) {
    $form['id'] = array('#type' => 'value', '#value' => $entity->id);
  }

  if ($op == 'edit') {
    global $user;

    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => 99,
      '#access' => user_access('administer entity_claim') || user_access("moderate entity_claim") || user_access("edit any $entity->bundle claims", $user),
    );

    // Claim options for administrators
    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Publishing options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#weight' => 90,
    );

    $form['options']['status'] = array(
      '#type' => 'radios',
      '#title' => t('Status'),
      '#options' => array(
        ENTITY_CLAIM_APPROVE => t('Approve'),
        ENTITY_CLAIM_REJECT => t('Reject'),
        ENTITY_CLAIM_PENDING => t('Pending'),
      ),
      '#default_value' => $entity->status,
    );

    if (variable_get('entity_claim_assign_role_edit', FALSE)) {
      $claimer = $form_state['claimer'] = user_load($entity->uid);

      $roles = array_map('check_plain', user_roles(TRUE));

      $form['options']['roles'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Claimer Roles'),
        '#default_value' => array_keys($claimer->roles),
        '#options' => $roles,
        //'#access' => $roles && user_access('administer permissions'),
      );

      $form['options']['roles'][DRUPAL_AUTHENTICATED_RID] = array(
        '#default_value' => TRUE,
        '#disabled' => TRUE,
      );

    }

    // Claim approved by informations.
    $form['author'] = array(
      '#type' => 'fieldset',
      '#title' => t('Authoring information'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#weight' => 95,
    );

    if (!empty($entity->updated_by)) {
      $update_user = user_load($entity->updated_by);
      $entity->author = $update_user->name;
    }

    $form['author']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Authored by'),
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => !empty($entity->author) ? $entity->author : $user->name,
      '#weight' => -1,
      '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
    );
  }

  $form['operation'] = array('#type' => 'value', '#value' => $op);
  $form['type'] = array('#type' => 'value', '#value' => $entity->entity_type);
  $form['entity_id'] = array('#type' => 'value', '#value' => $entity->entity_id);
  $form['uid'] = array('#type' => 'value', '#value' => $entity->uid);
  $form['bundle'] = array('#type' => 'value', '#value' => $entity->bundle);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  if ($op == 'edit') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('entity_claim_form_delete_submit'),
    );
  }

  $form['#entity'] = $form_state['entity'] = $entity;
  $form['#submit'][] = 'entity_claim_add_form_submit';

  return $form;
}

/**
 * @todo: Disable the roles(i.e. authenticated user, admin) to be check /
 * unchecked by anyuser.
 *
 * @param $element
 * @param $form_state
 *
 * @return mixed
 */
function entity_claim_disable_roles_checkbox($element, &$form_state) {
  //$entity = $form_state['entity'];
  return $element;
}


/**
 * Validation callback.
 * @param $form
 * @param $form_state
 */
function entity_claim_add_form_validate($form, &$form_state) {
  $claim = $form['#entity'];
  if (!$claim->is_new && $form_state['values']['operation'] != 'new') {
    if (empty($form_state['values']['name'])) {
      form_error($form['author']['name'], t('Please enter claim updated by author'));
    }
  }
}

/**
 * Submit callback for entity claim form.
 * @param $form
 * @param $form_state
 */
function entity_claim_add_form_submit($form, &$form_state) {
  // Get the default settings.
  $form_state['entity_type'] = 'claim';
  $form_state['claim'] = $claim = $form['#entity'];
  $values = $form_state['values'];
  entity_form_submit_build_entity('claim', $claim, $form, $form_state);
  $bundle = $values['bundle'];

  if (empty($claim->id) && !$claim->is_new) {
    $update_user = user_load_by_name(trim($form_state['values']['name']));
    $claim->updated_by = $update_user->uid;
  }

  $insert = empty($claim->id);
  // Save entity claim.
  entity_claim_save($claim, $values, $values['operation']);

  $claim_link = l(t('view'), 'claim/' . $claim->id);
  $watchdog_args = array('%title' => $claim->label);
  $t_args = array('%title' => $claim->entity_label);

  if ($insert) {
    watchdog('claim', 'Claim created - %title.', $watchdog_args, WATCHDOG_NOTICE, $claim_link);
    drupal_set_message(t('Your claim request for %title is created successfully.', $t_args));
  }
  else {
    watchdog('claim', 'Claim updated %title.', $watchdog_args, WATCHDOG_NOTICE, $claim_link);
    drupal_set_message(t('claim request for %title has been updated.', $t_args));
  }

  // Redirect to defined path.
  $redirect = variable_get('entity_claim_' . $bundle . '_redirect', '');
  $redirect = token_replace($redirect, array('claim' => $claim));
  $form_state['redirect'] = !empty($redirect) ? $redirect : "<front>";

}

/**
 * Form submission handler for claim delete.
 *
 * Handles the 'Delete' button on the claim form.
 *
 * @see entity_claim_form()
 * @see entity_claim_add_form_validate()
 */
function entity_claim_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $claim = $form['#entity'];
  $form_state['redirect'] = array('claim/' . $claim->id . '/delete', array('query' => $destination));
}

/**
 * Form constructor for the claim deletion confirmation form.
 *
 * @see entity_claim_delete_form_submit()
 */
function entity_claim_delete_form($form, &$form_state, $entity) {
  $form['#entity'] = $entity;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['cid'] = array('#type' => 'value', '#value' => $entity->id);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $entity->label)),
    'claim/' . $entity->id,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes claim deletion.
 *
 * @see entity_claim_delete_form()
 */
function entity_claim_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    module_load_include('inc', 'entity_claim', 'includes/entity_claim.helper');
    $claim = $form['#entity'];

    if ($claim->isDefaultRevision()) {
      entity_claim_claim_delete_multiple(array($form_state['values']['cid']));
      watchdog('claim', '%title deleted.', array('%title' => $claim->label));
      drupal_set_message(t('%title has been deleted.', array('%title' => $claim->label)));
    }
    else {
      entity_revision_delete('claim', $claim->vid);
      drupal_set_message(t('Revision %label has been deleted.', array('%label' => $claim->label)));
      watchdog('claim', 'Deleted claim revision %label.', array('%label' => $claim->label), WATCHDOG_NOTICE);
    }
    $form_state['redirect'] = '<front>';
  }
}

