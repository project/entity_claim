<?php
/**
 * @file
 * Helper functions for entity_claim module.
 */


function entity_claim_get_settings() {
  return array(
    'active_bundles' => variable_get('entity_claim_active_bundles', array()),
    'display_message' => variable_get('entity_claim_display_message', TRUE),
    'redirect_path' => variable_get('entity_claim_claim_redirect', ''),
  );
}

/**
 * Insert claim data to table.
 * @param $data
 *
 * @throws \Exception
 */
function entity_claim_claim_delete_multiple($ids) {
  if (user_access('administer entity_claim') || user_access('moderate entity_claim')) {
    $ret = entity_get_controller('claim')->delete($ids);
    return $ret;
  }
  else {
    $entities = $ids ? entity_load('claim', $ids) : FALSE;
    foreach ($entities as $entity) {
      $entity->deleted = TRUE;
      entity_save('claim', $entity);
    }
  }
}

function entity_claim_load_claim_by_claim($uid, $entity_type, $entity_id) {
  $entities = entity_load('claim', FALSE, array('uid' => $uid, 'entity_type' => $entity_type, 'entity_id' => $entity_id));
  return $entities;
}

function entity_claim_is_entity_claimed($entity_bundle, $entity_id) {
  $entities = entity_load('claim', FALSE, array('bundle' => $entity_bundle, 'entity_id' => $entity_id, 'status' => ENTITY_CLAIM_APPROVE));
  return sizeof($entities) > 0;
}

/**
 * Update existing claim data.
 * @param $data
 * @param $condition
 *
 * @throws \Exception

function entity_claim_claim_update($data, $conditions) {
  $query = db_update('entity_claim');
  $query->fields($data);
  foreach($conditions as $key => $value) {
    $query->condition($key, $value);
  }
  $query->execute();
}
*/

/**
 * Update existing claim status.
 * @param $data
 * @param $claim_id
 *
 * @throws \Exception
 */
function entity_claim_update_status($status, $claim_id) {

  $up_status = '';
  switch ($status) {
    case 'approve':
      $up_status = ENTITY_CLAIM_APPROVE;
      break;
    case 'reject':
      $up_status = ENTITY_CLAIM_REJECT;
      break;
    case 'pending':
      $up_status = ENTITY_CLAIM_PENDING;
      break;
//    case 'processing':
//      break;
  }

  if ($up_status) {
    $query = db_update('entity_claim');
    $query->fields(array('status' => $up_status));
    $query->condition('id', $claim_id);
    $query->execute();
  }
}
