<?php

/**
 * @file
 * Functions, related with block API.
 */

/**
 * Implements hook_block_info().
 */
function entity_claim_block_info() {
  $blocks = array();
  $entities_claimed = entity_claim_enabled_bundles();

  foreach ($entities_claimed['bundles'] as $bundle) {
    $blocks['entity_claim_' . $bundle] = array(
      'info' => t('Entity Claim - ' . $bundle),
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function entity_claim_block_configure($delta = '') {
  $form = array();

  if (strpos($delta, 'entity_claim_') !== FALSE) {
    $form['entity_claim_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => entity_claim_text($delta),
      '#description' => t('This field supports <a href="https://www.drupal.org/project/token" target="_blank">tokens</a> if installed.'),
    );

    if (module_exists('token')) {
      $form['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('claim', 'node', 'user', 'taxonomy_term'),
        '#global_types' => TRUE,
        '#dialog' => TRUE,
      );
    }

    // Keep it simple for the admins, using select list.
    $form['entity_claim_target'] = array(
      '#type' => 'select',
      '#options' => array(
        '_self' => t('Self'),
        '_blank' => t('Blank'),
        '_top' => t('Top'),
        '_parent' => t('Parent'),
      ),
      '#title' => t('Link window target'),
      '#default_value' => entity_claim_target($delta),
    );

    $form['entity_claim_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Link extra classes'),
      '#default_value' => entity_claim_class($delta),
    );
  }

  return $form;
}

/**
 * Implements hook_block_save().
 */
function entity_claim_block_save($delta = '', $edit = array()) {
  if (strpos($delta, 'entity_claim_') !== FALSE) {
    variable_set($delta . '_text', $edit['entity_claim_text']);
    // Update the i18n string if need.
    if (function_exists('i18n_string_update')) {
      $name = array(
        'entity_claim',
        'entity_claim_link',
        'entity_claim_text',
        'text',
      );
      i18n_string_update($name, $edit['entity_claim_text']);
    }
    variable_set($delta . '_target', $edit['entity_claim_target']);
    variable_set($delta . '_class', $edit['entity_claim_class']);
  }
}

/**
 * Implements hook_block_view().
 */
function entity_claim_block_view($delta = '') {
  $block = array();

  if (strpos($delta, 'entity_claim_') !== FALSE) {
    $text = variable_get($delta . '_text', t('Claim entity'));
    $target = variable_get($delta . '_target', '_self');
    $class = variable_get($delta . '_class', '');

    $entity = menu_get_object();
    // If $entity is null, then no support.
    if (isset($entity)) {
      // Support all entity_types, Get the entity type from zeroth arg.
      $entity_type = arg(0);

      // Add support for entity specific title using tokens.
      $text = token_replace($text, array($entity_type => $entity));

      $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
      $type = str_replace('entity_claim_', '', $delta);

      if ($type == $entity_wrapper->getBundle()) {
        $claim_link = 'claim/add/' . $entity_type . '/' . $entity_wrapper->getIdentifier();

        $link_options = array();
        $link_options['attributes']['class'][] = $class;
        $link_options['attributes']['target'] = $target;

        // Allow other modules to add classes, allow html, etc.
        drupal_alter('entity_claim_link_options', $link_options, $entity_wrapper);

        if (entity_claim_add_access($entity_type, $entity_wrapper->getIdentifier())) {
          if (user_is_anonymous()) {
            $link_options['query'] = array('destination' => $claim_link);
            $link = l($text, 'user/login', $link_options);
          }
          else {
            // Get claim bundle.
            $claim_bundle = _entity_claim_prepare_bundle_name($entity_type, $entity->type);
            // Get redirect from claim.
            $redirect = variable_get('entity_claim_' . $claim_bundle . '_redirect', '');
            // Add support for entity specific title using tokens.
            $redirect = token_replace($redirect, array($entity_type => $entity));
            $link_options['query'] = array('destination' => $redirect);
            $link = l($text, $claim_link, $link_options);
          }
          $block['content'] = $link;
        }
      }
    }
  }

  return $block;
}

/**
 * Returns text for entity claim link.
 */
function entity_claim_text($delta) {
  $text = variable_get($delta . '_text', t('Claim entity'));
  if (function_exists('i18n_string_translate')) {
    $text = i18n_string_translate(
      array(
        'entity_claim',
        $delta,
        $delta . '_text',
        'text',
      ),
      $text
    );
  }
  return $text;
}

/**
 * Returns target for entity claim link.
 */
function entity_claim_target($delta) {
  $target = variable_get($delta . '_target', '_self');
  return $target;
}

/**
 * Returns entity claim link class.
 */
function entity_claim_class($delta) {
  $class = variable_get($delta . '_class', '');
  return $class;
}
