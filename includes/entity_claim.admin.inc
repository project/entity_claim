<?php
/**
 * @file
 * administration relation functions of entity_claim.
 */

/**
 * @TODO: Remove commented part once confirmed.
 * Admin configurationn form.
 * @param $forn
 * @param $form_state
 *
 * @return mixed
 */
function entity_claim_general_settings_form($forn, &$form_state) {

  $options = array();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_type != 'claim') {
      $options[$entity_type] = array();
      foreach ($entity_info['bundles'] as $bundle_name => $bundle) {
        $options[$entity_type][$entity_type . '||' . $bundle_name] = $bundle_name;
      }
    }
  }
  /*
  $form['entity_claim_general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['entity_claim_general_settings']['entity_claim_display_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display success message after claim form submitted.'),
    '#default_value' => variable_get('entity_claim_display_message', TRUE),
  );

  $form['entity_claim_general_settings']['entity_claim_claim_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect path'),
    '#size' => 40,
    '#default_value' => variable_get('entity_claim_claim_redirect', ''),
  );
  */

  $form['entity_claim_entity_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['entity_claim_entity_settings']['entity_claim_active_bundles'] = array(
    '#type' => 'select',
    '#title' => t('Select entity bundles'),
    '#description' => t('Select entity bundles for which you want to display claim button'),
    '#options' => $options,
    '#size' => 10,
    '#default_value' => variable_get('entity_claim_active_bundles', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'] = array('system_settings_form_submit', 'entity_claim_general_settings_form_submit');
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}

/**
 * Clear cache after configurations save to access the manage fields.
 */
function entity_claim_general_settings_form_submit($form, &$form_state) {
  drupal_flush_all_caches();
}

/**
 * Generates the claim manage form.
 */
function entity_claim_manage_list($form, &$form_state) {

  // Get all enabled entity bundles.
  $enabled_bundles = entity_claim_enabled_bundles();
  $entities_info = entity_get_info();

  $header = array(t('Bundle'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();
  foreach ($entities_info as $entity_type => $entity_info) {
    if ($entity_type != 'claim' && in_array($entity_type, $enabled_bundles['entities'])) {
      foreach ($enabled_bundles['raw'][$entity_type] as $bundle_name => $bundle_name) {
        //if (in_array($bundle_name, $enabled_bundles['bundles'])) {
          $claim_bundle_name = $entity_type . '__' . $bundle_name;

          $rows[] = array(
            $entity_info['label'] .': '. $bundle_name,
            l(t('Edit'), "admin/structure/claims/manage/$claim_bundle_name/settings"),
            l(t('Manage fields'), "admin/structure/claims/manage/$claim_bundle_name/fields"),
            l(t('Manage display'), "admin/structure/claims/manage/$claim_bundle_name/display"),
          );
       // }
      }
    }
  }
  $form['table'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  return $form;
}

function entity_claim_manage_bundle_settings_form($form, &$form_state, $bundle) {

  $form['entity_claim_author'] = array(
    '#type' => 'fieldset',
    '#title' => t('Make Author'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['entity_claim_author']['entity_claim_make_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('Assign claimer as an author of claimed entity.'),
    '#default_value' => variable_get('entity_claim_make_author', FALSE),
    '#description' => t('Select checkbox if you want to assign new roles to claimer on status update.'),
  );

  list($entity_type, $bundle_name) = explode('__', $bundle);
  $entity_info = entity_get_info($entity_type);

  $form['entity_claim_author']['entity_claim_make_author_field'] = array(
    '#type' => 'select',
    '#title' => t('Select author field of entity.'),
    '#options' => drupal_map_assoc($entity_info['schema_fields_sql']['base table']),
    '#default_value' => variable_get('entity_claim_make_author_field', ''),
    '#description' => t('Select the field that stores the author information of entity.'),
    '#states' => array(
      'visible' => array(
        ':input[name="entity_claim_make_author"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['entity_claim_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Assign Role'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['entity_claim_role']['entity_claim_assign_role_edit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Assign new role to claimer on edit page.'),
    '#default_value' => variable_get('entity_claim_assign_role_edit', FALSE),
    '#description' => t('Select checkbox if you want to allow auther to apply roles to claimer on status update.'),
  );

  $form['entity_claim_role']['entity_claim_assign_role'] = array(
    '#type' => 'checkbox',
    '#title' => t('Assign roles to user when entity status updated.'),
    '#default_value' => variable_get('entity_claim_assign_role', FALSE),
    '#description' => t('Select checkbox if you want to assign new roles to claimer on status update.'),
  );

  $form['entity_claim_role']['entity_claim_assign_role_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Assign Roles'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="entity_claim_assign_role"]' => array('checked' => TRUE),
      ),
    ),
  );

  $statuses = array(
    ENTITY_CLAIM_APPROVE => t('Approve'),
    ENTITY_CLAIM_REJECT => t('Reject'),
    ENTITY_CLAIM_PENDING => t('Pending')
  );
  foreach($statuses as $status => $title) {
    $form['entity_claim_role']['entity_claim_assign_role_fieldset']['entity_claim_' . $bundle . '_claim_roles_' . $status] = array(
      '#type' => 'checkboxes',
      '#title' => t('Roles for status - %status', array('%status' => $title)),
      '#options' => user_roles(),
      '#default_value' => variable_get('entity_claim_' . $bundle . '_claim_roles_' . $status, array()),
      '#description' => t('Select role to assign to the claimer after status update, Leave empty otherwise.'),
    );
  }

  $form['entity_claim_' . $bundle . '_multiple_submission_limited'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit multiple submission from claimer.'),
    '#default_value' => variable_get('entity_claim_' . $bundle . '_multiple_submission_limited', FALSE),
    '#description' => t('Select checkbox if you want to limit submission for claim.'),
  );

  $form['entity_claim_' . $bundle . '_remove_link_claimed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove claim link if entity claim approved.'),
    '#default_value' => variable_get('entity_claim_' . $bundle . '_remove_link_claimed', FALSE),
    '#description' => t('Select checkbox if you want to remove claim link after entity claim approved.'),
  );

  $form['entity_claim_' . $bundle . '_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter path to redirect after submission of claim'),
    '#default_value' => variable_get('entity_claim_' . $bundle . '_redirect', ''),
    '#description' => t('Leave empty if you want to redirect to front page and show a status message. This field supports <a href="https://www.drupal.org/project/token" target="_blank">tokens</a> if installed.'),
  );

  if (module_exists('token')) {
    $form['tokens'] = array(
      '#theme' => 'token_tree',
      '#token_types' => array('claim'),
      '#global_types' => TRUE,
      '#dialog' => TRUE,
    );
  }

  return system_settings_form($form);
}
