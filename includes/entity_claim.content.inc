<?php

/**
 * Page callback: Claim view page.
 */
function entity_claim_view_claim($entity) {
  return entity_view('claim', array($entity));
}

/**
 * Menu callback; present an administrative claim listing.
 */
function entity_claim_content($type) {
  $edit = $_POST;

  if (isset($edit['operation']) && isset($edit['claims']) && $edit['claims']) {
    return drupal_get_form('entity_claim_multiple_operation_confirm', $edit['operation']);
  }
  else {
    return drupal_get_form('entity_claim_content_overview', $type);
  }
}

/**
 * Form builder for the claim overview administration form.
 *
 * @param $arg
 */
function entity_claim_content_overview($form, &$form_state, $arg) {
  // Build an 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );

  $options = array(
    '' => t('Select'),
    'approve' => t('Approve the selected claims'),
    'reject' => t('Reject the selected claims'),
    'delete' => t('Delete the selected claims'),
  );

  if ($arg == 'approved') {
    unset($options['approve']);
  }
  elseif ($arg == 'rejected') {
    unset($options['reject']);
  }

  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'publish',
  );

  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $header = array(
    'entity_id' => array('data' => t('Claimed For'), 'field' => 'entity_id'),
    'entity_type' => array('data' => t('Entity type'), 'field' => 'entity_type'),
    'bundle' => array('data' => t('Bundle')),
    'uid' => array('data' => t('User'), 'field' => 'uid'),
    'approved_by' => array('data' => t('Approved by'), 'field' => 'updated_by'),
    'created' => array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc'),
    'deleted' => array('data' => t('Deleted')),
    'view' => array('data' => t('view')),
    'edit' => array('data' => t('Edit')),
    'delete' => array('data' => t('Delete')),
  );

  $query = db_select('entity_claim', 'ec')->extend('PagerDefault')->extend('TableSort');
  //$query->condition('ec.deleted', 0);

  if ($arg == 'pending') { // Pending.
    $query->join('entity_claim_revision', 'ecr', 'ecr.id = ec.id AND (ecr.vid <> ec.vid OR ec.status = 2)');
  }
  elseif ($arg == 'rejected') { // Rejected.
    $query->join('entity_claim_revision', 'ecr', 'ecr.id = ec.id AND (ecr.vid <> ec.vid OR ec.status = 0)');
  }
  else { // Approved.
    $query->join('entity_claim_revision', 'ecr', 'ecr.id = ec.id AND ecr.vid = ec.vid');
    $query->condition('ec.status', 1);
  }

  $query->fields('ecr', array('vid'));

  $query->limit(50);
  $query->orderByHeader($header);
  $result = $query->execute()->fetchCol();
  // Build a table listing the appropriate claims.
  $options = array();

  foreach ($result as $vid) {
    $claim = entity_revision_load('claim', $vid);
    $claim_uri = entity_uri('claim', $claim);

    $entities = entity_load($claim->entity_type, array($claim->entity_id));
    $entity = $entities[$claim->entity_id];
    $entity_wrapper = entity_metadata_wrapper($claim->entity_type, $entity);

    $entity_info = $entity_wrapper->entityInfo();
    $entity_uri = entity_uri($claim->entity_type, $entity);

    $options[$claim->id] = array(
      'entity_id' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $entity_wrapper->label(),
          '#href' => $entity_uri['path'],
        ),
      ),
      'entity_type' => $entity_info['label'],
      'bundle' => $entity_wrapper->getBundle(),
      'uid' => theme('username', array('account' => user_load($claim->uid))),
      'approved_by' => !empty($claim->updated_by) ? theme('username', array('account' => user_load($claim->updated_by))) : '',
      'created' => format_date($claim->created, 'short'),
      'deleted' => ($claim->deleted) ? t('Yes') : t('No'),
      'view' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('view'),
          '#href' => $claim_uri['path'],
        ),
      ),
      'edit' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Edit'),
          '#href' => 'claim/' . $claim->id . '/edit',
          '#options' => array('query' => array('destination' => 'admin/content/claims'))
        ),
      ),
      'delete' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('Delete'),
          '#href' => 'claim/' . $claim->id . '/delete',
          '#options' => array('query' => array('destination' => 'admin/content/claims'))
        ),
      ),
    );
  }

  $form['claims'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No claims available.'),
  );

  $form['pager'] = array('#theme' => 'pager');

  return $form;
}

/**
 * Validation: Validate entity_claim_content_overview form submissions.
 */
function entity_claim_content_overview_validate($form, &$form_state) {
  $form_state['values']['claims'] = array_diff($form_state['values']['claims'], array(0));
  // We can't execute any 'Update options' if no claims were selected.
  if (count($form_state['values']['claims']) == 0) {
    form_set_error('', t('Select one or more claims to perform the update on.'));
  }
}

/**
 * List the selected claims and verify that the admin wants to update / delete them.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @return
 *   TRUE if the claims should be processed, FALSE otherwise.
 */
function entity_claim_multiple_operation_confirm($form, &$form_state, $operation) {
  $edit = $form_state['input'];

  $form['claims'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );

  foreach ($edit['claims'] as $vid) {
    $claim = entity_revision_load('claim', $vid);
    $entity_wrapper = entity_metadata_wrapper('claim', $claim);

    $form['claims'][$claim->id] = array(
      '#type' => 'hidden',
      '#value' => $claim->id,
      '#prefix' => '<li>',
      '#suffix' => $entity_wrapper->label() . '</li>');
  }

  $form['operation'] = array('#type' => 'hidden', '#value' => $operation);

  switch ($operation) {
    case 'delete':
      $status_text = t('There do not appear to be any claims to delete, or your selected claim was deleted by another administrator.');
      $confirm_text = t('Are you sure you want to delete these claims?');
      $button_text = t('Delete claims');
      break;
    case 'approve':
      $status_text = t('There do not appear to be any claims to approve, or your selected claim was approved by another administrator.');
      $confirm_text = t('Are you sure you want to approve these claims?');
      $button_text = t('Approve claims');
      break;
    case 'reject':
      $status_text = t('There do not appear to be any claims to reject, or your selected claim was rejected by another administrator.');
      $confirm_text = t('Are you sure you want to reject these claims?');
      $button_text = t('Reject claims');
      break;
  }


  if (!count($edit['claims'])) {
    drupal_set_message($status_text);
    drupal_goto('admin/content/claims');
  }
  else {
    return confirm_form($form,
      $confirm_text,
      'admin/content/claims', t('This action cannot be undone.'),
      $button_text, t('Cancel'));
  }
}

/**
 * Process entity_claim_multiple_operation_confirm form submissions.
 */
function entity_claim_multiple_operation_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    if ($form_state['values']['operation'] == 'delete') {
      module_load_include('inc', 'entity_claim', 'includes/entity_claim.helper');
      foreach ($form_state['values']['claims'] as $id) {
        $claim = claim_load($id);

        // If claim is current - unpublish first.
        if ($claim->isDefaultRevision()) {
          $claim->unpublish();
        }
      }

      // Delete multiple claims.
      entity_claim_claim_delete_multiple($form_state['values']['claims']);

      cache_clear_all();
      $count = count($form_state['values']['claims']);

      watchdog('content', 'Deleted @count claims.', array('@count' => $count));
      drupal_set_message(format_plural($count, 'Deleted 1 claim.', 'Deleted @count claims.'));
    }
    else {
      global $user;

      if ($form_state['values']['operation'] == 'approve') {
        $status = ENTITY_CLAIM_APPROVE;
      }
      elseif ($form_state['values']['operation'] == 'reject') {
        $status = ENTITY_CLAIM_REJECT;
      }

      entity_claim_mass_update($form_state['values']['claims'], array(
        'updated_by' => $user->uid,
        'status' => $status
      ));
    }
  }

  $form_state['redirect'] = 'admin/content/claims';
}

