<?php
/**
 * @file
 * code for entity_claim module.
 */

// define('ENTITY_CLAIM_PROCESSING', 3);
define('ENTITY_CLAIM_APPROVE', 1);
define('ENTITY_CLAIM_REJECT', 0);
define('ENTITY_CLAIM_PENDING', 2);

require_once __DIR__ . '/includes/entity_claim.block.inc';

/**
 * Implements hook_views_api().
 */
function entity_claim_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'entity_claim') . '/views',
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function entity_claim_entity_info_alter(&$entities) {
  // Get all enabled entity bundles.
  $entities_claimed = entity_claim_enabled_bundles();

  foreach ($entities as $entity_type => $entity_info) {
    if ($entity_type != 'claim' && in_array($entity_type, $entities_claimed['entities'])) { // Skip itself.
      foreach ($entities_claimed['raw'][$entity_type] as $bundle_name) {
          $claim_bundle_name = _entity_claim_prepare_bundle_name($entity_type, $bundle_name);
          $entities['claim']['bundles'][$claim_bundle_name] = array(
            'label' => t('@entity_type claim', array('@entity_type' => $entity_info['label'])),
            'admin' => array(
              'path' => 'admin/structure/claims/manage/%claim_type',
              'real path' => "admin/structure/claims/manage/$claim_bundle_name",
              'bundle argument' => 4,
              'access callback' => 'entity_claim_administer_access',
              'access arguments' => array(4),
            )
          );
      }
    }
  }
}

/**
 * Implements hook_entity_info().
 */
function entity_claim_entity_info() {
  $info = array();

  $info['claim'] = array(
    'label' => t('Claim'),
    'plural label' => t('Claims'),
    'description' => t('An entity type used by entity_claim module.'),
    'entity class' => 'EntityClaim',
    'metadata controller class' => 'EntityClaimMetadataController',
    'controller class' => 'EntityClaimController',
    'views controller class' => 'EntityClaimViewsController',
    'base table' => 'entity_claim',
    'revision table' => 'entity_claim_revision',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'bundle' => 'bundle',
      'revision' => 'vid',
    ),
    'token type' => 'claim',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'entity_claim',
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label' => t('Full'),
        'custom settings' => FALSE,
      ),
    ),
    'access callback' => 'entity_claim_access',
  );

  return $info;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function entity_claim_entity_property_info_alter(&$info) {
  $properties = &$info['claim']['properties'];
  $properties['bundle']['label'] = t('Bundle');
}

/**
 * @todo Do not allow uninstall if any configured item has fields and data
 *   exist untill data and fields are deleted.
 *
 * Implements hook_system_info_alter().
 */
function entity_claim_system_info_alter(&$info, $file, $type) {
  /*if ($type == 'module' && module_hook($file->name, 'field_info')) {
    $fields = field_read_fields(array('module' => $file->name), array('include_deleted' => TRUE));
    if ($fields) {
      $info['required'] = TRUE;

      // Provide an explanation message (only mention pending deletions if there
      // remains no actual, non-deleted fields)
      $non_deleted = FALSE;
      foreach ($fields as $field) {
        if (empty($field['deleted'])) {
          $non_deleted = TRUE;
          break;
        }
      }
      if ($non_deleted) {
        if (module_exists('field_ui')) {
          $explanation = t('Field type(s) in use - see <a href="@fields-page">Field list</a>', array('@fields-page' => url('admin/reports/fields')));
        }
        else {
          $explanation = t('Fields type(s) in use');
        }
      }
      else {
        $explanation = t('Fields pending deletion');
      }
      $info['explanation'] = $explanation;
    }
  }*/
}

/**
 * Implements hook_theme().
 */
function entity_claim_theme($existing, $type, $theme, $path) {
  return array(
    'claim' => array(
      'render element' => 'elements',
      'path' => $path . '/templates',
      'template' => 'claim',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function entity_claim_menu() {
  $items = array();

  $items['admin/structure/claims'] = array(
    'title' => 'Entity Claims',
    'description' => t('Manage entity claim types, including adding and removing fields and the display of fields.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_manage_list'),
    'access arguments' => array('administer entity_claim'),
    'file' => 'includes/entity_claim.admin.inc',
  );

  $items['admin/structure/claims/add'] = array(
    'title' => 'Add New Entity Claim',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_general_settings_form'),
    'access arguments' => array('administer entity_claim'),
    'file' => 'includes/entity_claim.admin.inc',
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/structure/claims/manage/%claim_type/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_manage_bundle_settings_form', 4),
    'access callback' => 'entity_claim_administer_access',
    'access arguments' => array(4),
    'file' => 'includes/entity_claim.admin.inc',
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
  );

  $items['claim/add/%claim_type/%'] = array(
    'title' => 'claim entity',
    'title callback' => 'entity_claim_title_callback',
    'title arguments' => array(2, 3),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_add_form', 2, 3),
    'access callback' => 'entity_claim_add_access',
    'access arguments' => array(2, 3),
    'file' => 'includes/entity_claim.pages.inc',
  );

  $items['claim/%claim'] = array(
    'title callback' => 'entity_class_label',
    'title arguments' => array(1),
    'page callback' => 'entity_claim_view_claim',
    'page arguments' => array(1),
    'access callback' => 'entity_claim_access',
    'access arguments' => array('view', 1),
    'file' => 'includes/entity_claim.content.inc',
  );

  $items['claim/%/view'] = array(
    'title' => 'View',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['claim/%claim/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_form', 1, 'edit'),
    'access callback' => 'entity_claim_access',
    'access arguments' => array('edit', 1),
    'file' => 'includes/entity_claim.pages.inc',
    'weight' => -5,
    'type' => MENU_LOCAL_TASK,
  );

  $items['claim/%claim/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('entity_claim_delete_form', 1, 'edit'),
    'access callback' => 'entity_claim_access',
    'access arguments' => array('delete', 1),
    'file' => 'includes/entity_claim.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['admin/content/claims'] = array(
    'title' => 'Claims',
    'description' => 'List claims and the claim approval queue.',
    'page arguments' => array('pending'),
    'page callback' => 'entity_claim_content',
    'access arguments' => array('moderate entity_claim'),
    'file' => 'includes/entity_claim.content.inc',
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
  );

  $items['admin/content/claims/pending'] = array(
    'title' => 'Pending Claims',
    'page arguments' => array('pending'),
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/content/claims/rejected'] = array(
    'title' => 'Rejected Claims',
    'page arguments' => array('rejected'),
    'access arguments' => array('moderate entity_claim'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/content/claims/approved'] = array(
    'title' => 'Approved Claims',
    'page arguments' => array('approved'),
    'access arguments' => array('moderate entity_claim'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function entity_claim_permission() {
  $perms = array();

  $perms['administer entity_claim'] = array(
    'title' => t('Administer Entity claim module'),
    'description' => t('Allows to administer Entity claim module.'),
  );

  $perms['moderate entity_claim'] = array(
    'title' => t('Moderate Entity claim module'),
    'description' => t('Allows to moderate Entity claim module.'),
  );

  $perms["view any entity claims"] = array(
    'title' => t('View any Entity claims'),
    'description' => t('Allows to view any claims.'),
  );

  $entities_claimed = entity_claim_enabled_bundles();

  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (in_array($entity_type, $entities_claimed['entities'])) {
      foreach ($entities_claimed['raw'][$entity_type] as $bundle_name => $bundle_name) {
        //if (in_array($bundle_name, $entities_claimed['bundles'])) {
        $claim_bundle = _entity_claim_prepare_bundle_name($entity_type, $bundle_name);

        $perms["create $claim_bundle claims"] = array(
          'title' => t('%entity- %bundle : Create claims', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
          'description' => t('Allows to Claim for %entity: %bundle.', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
        );

        $perms["view $claim_bundle claims"] = array(
          'title' => t('%entity- %bundle : View claims', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
          'description' => t('Allows to view claims for %entity: %bundle.', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
        );

        $perms["edit own $claim_bundle claims"] = array(
          'title' => t('%entity- %bundle : Edit own claims', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
          'description' => t('Allows to edit own %entity: %bundle claims.', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
        );

        $perms["edit any $claim_bundle claims"] = array(
          'title' => t('%entity: %bundle : Edit any claims', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
          'description' => t('Allows to edit any %entity: %bundle claims.', array(
            '%entity' => $entity_info['label'],
            '%bundle' => $bundle_name,
          )),
        );
      }
    }
  }

  return $perms;
}

/**
 * Implements hook_entity_delete().
 */
function entity_claim_entity_delete($entity, $type) {
  if (isset($entity->nid)) {
    module_load_include('inc', 'entity_claim', 'includes/entity_claim.helper');

    $query = new EntityFieldQuery();
    $results = $query
      ->entityCondition('entity_type', 'claim')
      ->propertyCondition('entity_id', $entity->nid, '=')
      ->execute();

    if (!empty($results)) {
      $ids = array();
      foreach ($results['claim'] as $result) {
        $ids[] = $result->id;
      }

      $claims = claim_load_multiple($ids);
      foreach ($claims as $claim) {
        if ($claim->isDefaultRevision()) {
          entity_claim_claim_delete_multiple(array($claim->id));
        } else {
          entity_revision_delete('claim', $claim->vid);
        }
      }
    }
  }
}

/**
 * Callback for hook_menu.
 *
 * @param string $bundle
 *
 * @return bool
 */
function entity_claim_administer_access($bundle) {
  return !empty($bundle) && user_access("administer entity_claim");
}

/**
 * Callback for hook_entity_info & hook_menu.
 *
 * @param string $op
 * @param object $claim
 * @param object $account
 *
 * @return bool
 */
function entity_claim_access($op, $claim, $account = NULL) {
  if (is_numeric($claim)) {
    $claims = entity_load('claim', array($claim));
    $claim = $claims[$claim];
  }

  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }

  $is_owner = ($claim->uid === $account->uid);

  switch ($op) {
    case 'edit':
      return user_access("moderate entity_claim") || (user_access("edit any $claim->bundle claims", $account))
             || ($is_owner && user_access("edit own $claim->bundle claims", $account));

    case 'delete':
      return (user_access("edit any $claim->bundle claims", $account))
             || ($is_owner && user_access("edit own $claim->bundle claims", $account))
             || user_access('administer entity_claim');

    case 'view':
      return $claim
        ? user_access("view $claim->bundle claims", $account)
        : user_access("view any claims");

//  case 'approve':
//    return user_access("moderate entity_claim") && (!entity_revision_is_default('claim', $claim) || !(bool) $claim->status);
//  case 'reject':
//    return user_access("moderate entity_claim") && entity_revision_is_default('claim', $claim) && (bool) $claim->status;
    default:
      return FALSE;
  }
}

/**
 * Entity Claim add access.
 * @param $entity_type
 * @param $entity_id
 *
 * @return bool
 */
function entity_claim_add_access($entity_type, $entity_id) {
  if (!empty($entity_type) && !empty($entity_id)) {
    global $user;
    $entity_wrapper = entity_metadata_wrapper($entity_type, $entity_id);

    $claim_bundle_separator = $entity_type . '||' . $entity_wrapper->getBundle();
    $claim_bundle = _entity_claim_prepare_bundle_name($entity_type, $entity_wrapper->getBundle());

    module_load_include('inc', 'entity_claim', 'includes/entity_claim.helper');

    if (variable_get('entity_claim_' . $claim_bundle . '_remove_link_claimed', FALSE)) {
      $claimed = entity_claim_is_entity_claimed($claim_bundle, $entity_id);
      if ($claimed) {
        return false;
      }
    }

    if(variable_get('entity_claim_' . $claim_bundle . '_multiple_submission_limited', FALSE)) {
      global $user;

      $claims = entity_claim_load_claim_by_claim($user->uid, $entity_type, $entity_id);
      if (count($claims) > 0) {
        drupal_set_message(t('You have already submitted a claim for @entity', array('@entity' => $entity_wrapper->label())), 'warning', false);
        return FALSE;
      }
    }

    $access = module_invoke_all('entity_claim_add_access', $entity_type, $entity_id);
    $bundles = entity_claim_enabled_bundles(TRUE);
    $access[] = in_array($claim_bundle_separator, $bundles);
    $access[] = user_access("create $claim_bundle claims") && $user->uid;

    return !in_array(FALSE, $access);
  }
}

/**
 * Title callback for Entity claim form.
 *
 * @param $entity_type
 * @param $entity_id
 *
 * @return null|string
 */
function entity_claim_title_callback($entity_type, $entity_id) {
  $claim_entity = call_user_func($entity_type .'_load', $entity_id);

  if (!empty($claim_entity->label)) {
    $title = $claim_entity->label;
  }
  elseif (!empty($claim_entity->title)) {
    $title = $claim_entity->title;
  }

  return t('Claim for @entity_title', array('@entity_title' => $title));
}

/**
 * Fetch an entity claim object.
 *
 * @param $id
 *   Integer specifying the claim id.
 *
 * @return
 *   A fully-loaded entity_claim object or FALSE if it cannot be loaded.
 */
function claim_load($id) {
  $claims = claim_load_multiple(array($id));
  return $claims ? reset($claims) : FALSE;
}

/**
 * Load claim type
 * @param $claim_type
 *
 * @return mixed
 */
function claim_type_load($claim_type) {
  return !empty($claim_type) ? $claim_type : FALSE;
}

/**
 * Loads claim entities from the database.
 *
 * @param $ids
 *   An array of claim IDs.
 * @param $conditions
 * @param $reset
 *
 * @return
 *   An array of claim objects indexed by id.
 */
function claim_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('claim', $ids, $conditions, $reset);
}

/**
 * Fetch an entity claim object.
 *
 * @param $vid
 *   Integer specifying the claim vid.
 *
 * @return
 *   A fully-loaded entity_claim object or FALSE if it cannot be loaded.
 */
function claim_by_vid_load($vid) {
  return entity_revision_load('claim', $vid);
}

/**
 * Get the enabled entity type and bundles for entity claim.
 */
function entity_claim_enabled_bundles($array = FALSE) {
  $enabled_bundles = variable_get('entity_claim_active_bundles', array());
  if ($array) {
    return $enabled_bundles;
  }

  $entities = $bundles = array();
  if ($enabled_bundles) {
    foreach($enabled_bundles as $enables) {
      list($entity, $bundle) = explode('||', $enables);
      $entities[$entity][$bundle] = $bundle;
      $bundles[] = $bundle;
    }
  }

  $claimed_entities = array_keys($entities);
  $claimed_bundles = array_values($bundles);

  return array('raw' => $entities, 'entities' => $claimed_entities, 'bundles' =>  $claimed_bundles);
}

/**
 * Implements hook_entity_view().
 *
 * Add link to create new claims.
 */
function entity_claim_entity_view($entity, $type, $view_mode, $langcode) {
  global $user;
  $entity_claimed = entity_claim_enabled_bundles();
  if (in_array($type, $entity_claimed['entities']) && in_array($entity->type, $entity_claimed['bundles'])) {

    $entity_wrapper = entity_metadata_wrapper($type, $entity);
    //if (!(entity_claim_add_access($type, $entity_wrapper->getIdentifier()))) {
    if (!isset($entity->content['links'])) {
      $entity->content['links'] = array(
        '#theme'      => 'links',
        '#pre_render' => array('drupal_pre_render_links'),
        '#attributes' => array(
          'class' => array('links', 'inline'),
        )
      );
    }

    $entity_info = entity_get_info($type);
    $claim_bundle = _entity_claim_prepare_bundle_name($type, $entity->type);

    // If user is anonymous, we redirect the user to login with destination set.
    if (user_is_anonymous() && entity_claim_add_access($type, $entity_wrapper->getIdentifier())) {
      $entity->content['links']['entity_claim'] = array(
        '#links' => array(
          'entity_claim' => array(
            'title' => t('Claim this @bundle', array('@bundle' => $entity_info['bundles'][$entity->type]['label'])),
            'href'  => 'user/login',
            'query' => array('destination' => "claim/add/$type/" . $entity_wrapper->getIdentifier()),
          )
        ),
      );
    }
    elseif (entity_claim_add_access($type, $entity_wrapper->getIdentifier())) {
      $entity->content['links']['entity_claim'] = array(
        '#links' => array(
          'entity_claim' => array(
            'title' => t('Claim this @bundle', array('@bundle' => $entity_info['bundles'][$entity->type]['label'])),
            'href'  => "claim/add/$type/" . $entity_wrapper->getIdentifier(),
            'query' => array('destination' => current_path()),
          )
        ),
      );
    }
    // }
  }
}

/**
 * Implements hook_user_cancel().
 */
function entity_claim_user_cancel($edit, $account, $method) {
  switch ($method) {
    // @TODO Get Confirmation of what to do here.
    case 'user_cancel_block_unpublish':
      // Unpublish claims.
     /* $ids = db_select('entity_claim', 'c')
        ->fields('c', array('id'))
        ->condition('c.uid', $account->uid)
        ->execute()
        ->fetchCol();

      entity_claim_mass_update($ids, array('status' => 0));*/
      break;

    // @TODO Get Confirmation of what to do here.
    case 'user_cancel_reassign':
      // Anonymize claims.
      /*$ids = db_select('entity_claim', 'c')
        ->fields('c', array('id'))
        ->condition('c.uid', $account->uid)
        ->execute()
        ->fetchCol();*/

      //entity_claim_mass_update($ids, array('status' => 0));
      break;
  }
}

/**
 * @TODO It should delete the entity claim record, But should also unassign
 * the claim user from entity. but need to decide.
 *
 * Implements hook_user_delete().
 */
function entity_claim_user_delete($account) {
  // Delete entity_claim (current revisions).
//  $ids = db_select('entity_claim', 'c')
//    ->fields('c', array('id'))
//    ->condition('c.uid', $account->uid)
//    ->execute()
//    ->fetchCol();
//
//  if (!empty($ids)) {
//    entity_delete_multiple('claim', $ids);
//  }
}

/**
 * Make mass update of claims
 *
 * @param array $ids
 *   Array of entity_claim ids.
 * @param array $updates
 *   Array of key/value pairs with claims field names and the value to update.
 */
function entity_claim_mass_update($ids, $updates) {
  if (count($ids) > 10) {
    $batch = array(
      'operations' => array(
        array('_entity_claim_mass_update_batch_process', array($ids, $updates))
      ),
      'finished' => '_entity_claim_mass_update_batch_finished',
      'title' => t('Processing'),
      'error_message' => t('The update has encountered an error.'),
      'file' => drupal_get_path('module', 'entity_claim') . '/includes/entity_claim.batch.inc',
    );

    batch_set($batch);
  }
  else {
    foreach (entity_load('claim', $ids) as $claim) {
      _entity_claim_update($claim, $updates);
    }

    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Updates individual entity_claim when fewer than 10 are queued.
 *
 * @param object $claim
 *   An entity_claim object to update.
 * @param $updates
 *   Associative array of updates.
 *
 * @return object
 *   An updated entity_claim object.
 */
function _entity_claim_update($claim, $updates) {
  // For efficiency manually save the original claim before applying any changes.
  $claim->original = clone $claim;
  foreach ($updates as $name => $value) {
    $claim->$name = $value;
  }

  return entity_claim_save($claim, array(), 'edit');
}

/**
 * Prepare bundle name based on given entity type and bundle.
 * @param $entity_type
 * @param $bundle
 *
 * @return string
 */
function _entity_claim_prepare_bundle_name($entity_type, $bundle) {
  return $entity_type . '__' . $bundle;
}

/**
 * Save / Update halabol claim entity.
 * @param $claim
 * @param $values
 * @param string $op
 */
function entity_claim_save(&$claim, $values = array(), $op = 'new') {
  module_invoke_all('claim_presave', $claim);
  $claim->save();
  module_invoke_all('claim_'. $op, $claim, $values);
}

/**
 * Implements hook_claim_edit().
 */
function entity_claim_claim_edit($claim, $values) {
  if ($claim->status == 1 && variable_get('entity_claim_make_author', FALSE) &&
      $author_field = variable_get('entity_claim_make_author_field', '')) {
    $entity = entity_load_single($claim->entity_type, $claim->entity_id);
    $entity->{$author_field} = $claim->uid;
    entity_save($claim->entity_type, $entity);
  }

  if (variable_get('entity_claim_assign_role_edit', FALSE)) {
    if (!empty($values['user_role']) && array_filter($values['user_role']) != FALSE) {
      $claimer = user_load($claim->uid);
      $claimer->roles += _entity_claim_get_roles_with_title($values['user_role']);
    }
  }

  if (variable_get('entity_claim_assign_role', FALSE)) {
    $new_roles = variable_get('entity_claim_' . $claim->bundle . '_claim_roles_' . $claim->status, array());

    if (!empty($new_roles)) {
      $claimer = !empty($claimer) && is_object($claimer) ? $claimer : user_load($claim->uid);
      $claimer->roles +=  _entity_claim_get_roles_with_title($new_roles);
    }
  }

  if (!empty($claimer)) {
    user_save($claimer);
  }
}

/**
 * Return associative array with rid and role title for given rids.
 *
 * @param $new_roles
 *
 * @return array
 */
function _entity_claim_get_roles_with_title($new_roles) {
  $roles = user_roles();
  $new_roles = drupal_map_assoc(array_filter($new_roles));
  $new_roles = array_intersect_key($roles, $new_roles);
  return $new_roles;
}

/**
 * Return an associative array of claim bundles to be used as an options list.
 *
 * @return array
 *   Keyed by name with a label value.
 */
function entity_claim_bundle_options_list() {
  $options = array();
  $entity_claimed = entity_claim_enabled_bundles();

  foreach ($entity_claimed['raw'] as $entity_type => $bundles) {
    $options[$entity_type] = array();
    foreach($bundles as $bundle) {
      $key = _entity_claim_prepare_bundle_name($entity_type, $bundle);
      $options[$entity_type][$key] = $entity_type . ':' . $bundle;
    }
  }
  return $options;
}

/**
 * Callback to return an array of options for claim status.
 */
function entity_claim_status_options() {
  return array(
    ENTITY_CLAIM_APPROVE => t('Approved'),
    ENTITY_CLAIM_REJECT => t('Rejected'),
    ENTITY_CLAIM_PENDING => t('Pending'),
  );
}

/**
 * Callback to get $claim->user.
 */
function entity_claim_claimer_property_user_get(EntityClaim $claim, array $options, $property_name, $entity_type) {
  if (is_numeric($claim->uid)) {
    return user_load($claim->uid);
  }
}

/**
 * Callback to set $claim->user.
 */
function entity_claim_claimer_property_user_set(EntityClaim $claim, $name, $value, $langcode, $type, $info) {
  $claim->uid = $value;
}

/**
 * Callback to get $claim->updated_by.
 */
function entity_claim_updated_by_property_user_get(EntityClaim $claim, array $options, $property_name, $entity_type) {
  if (is_numeric($claim->updated_by)) {
    return user_load($claim->updated_by);
  }
}

/**
 * Callback to set $claim->updated_by.
 */
function entity_claim_updated_by_property_user_set(EntityClaim $claim, $name, $value, $langcode, $type, $info) {
  $claim->updated_by = $value;
}

/**
 * Callback to load entity data.
 */
function entity_claim_entity_property_source_get(EntityClaim $claim, $name, $value, $langcode, $type, $info) {
  $entity = entity_load_single($claim->entity_type, $claim->entity_id);
  return ($entity) ? entity_metadata_wrapper($claim->entity_type, $entity) : NULL;
}
