<?php

/**
 * Implements hook_views_data_alter().
 */
function entity_claim_views_data_alter(&$data) {
  $data['entity_claim']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Claim'),
    'help' => t('Entity Claim table'),
  );

  $data['entity_claim']['table']['default_relationship'] = array(
    'entity_claim_revision' => array(
      'table' => 'entity_claim_revision',
      'field' => 'vid',
    ),
  );

  $data['entity_claim']['table']['join'] = array(
    'users' => array(
      'table' => 'users',
      'field' => 'uid',
    ),
  );

  // Entity claim field : id
  $data['entity_claim']['id']['field'] = array(
    'handler' => 'entity_claim_views_handler_field_id',
    'click sortable' => TRUE,
  );
  $data['entity_claim']['id']['argument'] = array(
    'handler' => 'views_handler_argument_numeric',
    'numeric' => TRUE,
  );
  $data['entity_claim']['id']['filter'] = array(
    'handler' => 'views_handler_filter_numeric',
  );
  $data['entity_claim']['id']['sort'] = array(
    'handler' => 'views_handler_sort',
  );

  // Entity claim field : vid
  $data['entity_claim']['vid']['field'] = array(
    'handler' => 'views_handler_field_numeric',
    'click sortable' => TRUE,
  );
  $data['entity_claim']['vid']['argument'] = array(
    'handler' => 'views_handler_argument_numeric',
    'numeric' => TRUE,
  );
  $data['entity_claim']['vid']['filter'] = array(
    'handler' => 'views_handler_filter_numeric',
  );
  $data['entity_claim']['vid']['sort'] = array(
    'handler' => 'views_handler_sort',
  );

  $data['entity_claim']['uid'] = array(
    'group' => t('Claimer'),
    'title' => t('Claimer UID'),
    'help' => t('The user id associated with this claim'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Claimer'),
      'title' => t('Claimer'),
      'help' => t('The user associated with this claim'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['entity_claim']['updated_by'] = array(
    'group' => t('Updated By'),
    'title' => t('User Updated by UID'),
    'help' => t('The author updated this claim'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Updated by'),
      'title' => t('Updated by User'),
      'help' => t('The author updated this claim'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['entity_claim']['view_node'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the claim.'),
      'handler' => 'entity_claim_views_handler_field_claim_link',
    ),
  );

  $data['entity_claim']['edit_node'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the claim.'),
      'handler' => 'entity_claim_views_handler_field_claim_link_edit',
    ),
  );

  $data['entity_claim']['delete_node'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the claim.'),
      'handler' => 'entity_claim_views_handler_field_claim_link_delete',
    ),
  );

  // Created date field.
  $data['entity_claim']['created']['filter'] = array(
    'handler' => 'views_handler_filter_date',
  );
  $data['entity_claim']['created']['sort'] = array(
    'handler' => 'views_handler_sort_date',
  );


  // Updated date field.
  $data['entity_claim']['updated']['filter'] = array(
    'handler' => 'views_handler_filter_date',
  );
  $data['entity_claim']['updated']['sort'] = array(
    'handler' => 'views_handler_sort_date',
  );

  // Status field.
  $data['entity_claim']['status']['field'] = array(
    'handler' => 'entity_claim_views_handler_field_claim_status',
    'click sortable' => TRUE,
  );
  $data['entity_claim']['status']['filter'] = array(
    'handler' => 'entity_claim_views_handler_filter_claim_status',
    'label' => t('Published'),
    'type' => 'rejected-approved-pending',
    'use equal' => TRUE,
  );
  $data['entity_claim']['status']['sort'] = array(
    'handler' => 'views_handler_sort',
  );

  // Entity claim field : deleted
  $data['entity_claim']['deleted']['argument'] = array(
    'handler' => 'views_handler_field_boolean',
    'click sortable' => TRUE,
  );
  $data['entity_claim']['deleted']['filter'] = array(
    'handler' => 'views_handler_filter_boolean_operator',
    'label' => t('Deleted'),
    'type' => 'yes-no',
    'use equal' => TRUE, // Use deleted = 1 instead of deleted <> 0 in WHERE statment
  );
  $data['entity_claim']['deleted']['sort'] = array(
    'handler' => 'views_handler_sort',
  );


  // Get all enabled entity bundles.
  $entities_claimed = entity_claim_enabled_bundles();

  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_type != 'claim' && in_array($entity_type, $entities_claimed['entities'])) { // Skip itself.
      $table = $entity_info['base table'];

      $data['entity_claim'][$table] = array(
        'title' => t('%label claim', array('%label' => $entity_info['label'])),
        'group' => t('Claim'),
        'help' => t('Claimed For %label.', array('%label' => $entity_info['label'])),
        'relationship' => array(
          'base' => $table,
          'base field' => $entity_info['entity keys']['id'], // The name of the field on the joined table.
          'field' => 'entity_id',
          'handler' => 'views_handler_relationship',
          'label' => $entity_info['label'],
          'title' => $entity_info['label'],
          'extra' => array(
            array(
              'table' => 'entity_claim',
              'field' => 'entity_type',
              'value' => $entity_type,
            )
          )
        ),
      );
    }
  }
}

