<?php

/**
 * @file
 * Contains the basic 'claim' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a claim.
 * Definition terms:
 * - link_to_claim default: Should this field have the checkbox "link to claim" enabled by default.
 *
 * @ingroup views_field_handlers
 */
class entity_claim_views_handler_field_id extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Don't add the additional fields to groupby
    if (!empty($this->options['link_to_claim'])) {
      $this->additional_fields['claim_id'] = array('table' => 'entity_claim', 'field' => 'id');
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_claim'] = array('default' => isset($this->definition['link_to_claim default']) ? $this->definition['link_to_claim default'] : FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    $form['link_to_claim'] = array(
      '#title' => t('Link this field to the original piece of content'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_claim']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the claim.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_claim']) && !empty($this->additional_fields['claim_id'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "claim/" . $this->get_value($values, 'claim_id');
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }

}
