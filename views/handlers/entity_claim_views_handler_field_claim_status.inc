<?php

/**
 * @file
 * Definition of entity_claim_views_handler_field_claim_status.
 */

/**
 * A handler to provide proper displays for claim status.
 *
 * Allows for display of pending, approve, rejected.
 *
 * @ingroup views_field_handlers
 */
class entity_claim_views_handler_field_claim_status extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => 'rejected-approved-pending');
    $options['type_custom_true'] = array('default' => '', 'translatable' => TRUE);
    $options['type_custom_false'] = array('default' => '', 'translatable' => TRUE);
    $options['type_custom_other'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function init(&$view, &$options) {
    parent::init($view, $options);

    $default_formats = array(
      'rejected-approved-pending' => array( t('Rejected'), t('Approved'),  t('Pending')),
    );
    $output_formats = isset($this->definition['output formats']) ? $this->definition['output formats'] : array();
    $custom_format = array('custom' => array(t('Custom')));
    $this->formats = array_merge($default_formats, $output_formats, $custom_format);
  }

  function options_form(&$form, &$form_state) {
    foreach ($this->formats as $key => $item) {
      $options[$key] = implode('/', $item);
    }

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Output format'),
      '#options' => $options,
      '#default_value' => $this->options['type'],
    );

    $form['type_custom_true'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom output for Approved'),
      '#default_value' => $this->options['type_custom_true'],
      '#states' => array(
        'visible' => array(
          'select[name="options[type]"]' => array('value' => 'custom'),
        ),
      ),
    );

    $form['type_custom_false'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom output for Rejected'),
      '#default_value' => $this->options['type_custom_false'],
      '#states' => array(
        'visible' => array(
          'select[name="options[type]"]' => array('value' => 'custom'),
        ),
      ),
    );

    $form['type_custom_other'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom output for Pending'),
      '#default_value' => $this->options['type_custom_other'],
      '#states' => array(
        'visible' => array(
          'select[name="options[type]"]' => array('value' => 'custom'),
        ),
      ),
    );

    /*$form['not'] = array(
      '#type' => 'checkbox',
      '#title' => t('Reverse'),
      '#description' => t('If checked, true will be displayed as false.'),
      '#default_value' => $this->options['not'],
    );*/
    parent::options_form($form, $form_state);
  }

  function render($values) {
    $value = $this->get_value($values);
    /*if (!empty($this->options['not'])) {
      $value = !$value;
    }*/

    if ($this->options['type'] == 'custom') {
      if ($value == 1) {
        return filter_xss_admin($this->options['type_custom_true']);
      }
      elseif ($value == 2) {
        return filter_xss_admin($this->options['type_custom_other']);
      }
      else {
        return filter_xss_admin($this->options['type_custom_false']);
      }
    }
    else  {
      $type = isset($this->formats[$this->options['type']]) ? $this->options['type'] : 'rejected-approved-pending';
      if ($value == 1) {
        return $this->formats[$type][1];
      }
      elseif ($value == 2) {
        return $this->formats[$type][2];
      }
      else {
        return $this->formats[$type][0];
      }
    }
  }



}
