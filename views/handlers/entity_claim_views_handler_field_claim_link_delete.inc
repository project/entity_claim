<?php

/**
 * @file
 * Definition of entity_claim_views_handler_field_claim_link_delete.
 */

/**
 * Field handler to present a link claim edit.
 *
 * @ingroup views_field_handlers
 */
class entity_claim_views_handler_field_claim_link_delete extends entity_claim_views_handler_field_claim_link {

  /**
   * Renders the link.
   */
  function render_link($claim, $values) {
    // Ensure user has access to delete this claim.
    if (!entity_claim_access('delete', $claim)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "claim/$claim->id/delete";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    return $text;
  }
}
