<?php

/**
 * @file
 * Definition of entity_claim_views_handler_field_claim_link_edit.
 */

/**
 * Field handler to present a link claim edit.
 *
 * @ingroup views_field_handlers
 */
class entity_claim_views_handler_field_claim_link_edit extends entity_claim_views_handler_field_claim_link {

  /**
   * Renders the link.
   */
  function render_link($claim, $values) {
    // Ensure user has access to edit this claim.
    if (!entity_claim_access('edit', $claim)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "claim/$claim->id/edit";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    return $text;
  }
}
